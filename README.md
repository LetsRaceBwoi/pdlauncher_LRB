# pdlauncher_LRB
A custom launcher written in C# for Project Dollhouse.

## Features
* Launches the game
* Settings changes in a single click
* Updating (WIP)
* City server status
* Custom UI
* Animations
