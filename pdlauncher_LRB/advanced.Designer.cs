﻿namespace pdlauncher_LRB
{
    partial class advanced
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.portLabel = new System.Windows.Forms.Label();
            this.portBox = new System.Windows.Forms.TextBox();
            this.toDefaultButton = new System.Windows.Forms.Button();
            this.pathBox = new System.Windows.Forms.TextBox();
            this.pathLabel = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.langList = new System.Windows.Forms.ComboBox();
            this.ipAddrBox = new System.Windows.Forms.TextBox();
            this.resLabel = new System.Windows.Forms.Label();
            this.resList = new System.Windows.Forms.ComboBox();
            this.ipLabel = new System.Windows.Forms.Label();
            this.langLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.launchButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // portLabel
            // 
            this.portLabel.AutoSize = true;
            this.portLabel.BackColor = System.Drawing.Color.Transparent;
            this.portLabel.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.portLabel.ForeColor = System.Drawing.Color.Black;
            this.portLabel.Location = new System.Drawing.Point(12, 87);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(37, 20);
            this.portLabel.TabIndex = 27;
            this.portLabel.Text = "Port:";
            // 
            // portBox
            // 
            this.portBox.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.portBox.ForeColor = System.Drawing.Color.Black;
            this.portBox.Location = new System.Drawing.Point(11, 109);
            this.portBox.Name = "portBox";
            this.portBox.Size = new System.Drawing.Size(500, 27);
            this.portBox.TabIndex = 26;
            this.portBox.Tag = "";
            // 
            // toDefaultButton
            // 
            this.toDefaultButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(120)))), ((int)(((byte)(174)))));
            this.toDefaultButton.FlatAppearance.BorderSize = 0;
            this.toDefaultButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toDefaultButton.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.toDefaultButton.ForeColor = System.Drawing.Color.White;
            this.toDefaultButton.Location = new System.Drawing.Point(398, 142);
            this.toDefaultButton.Name = "toDefaultButton";
            this.toDefaultButton.Size = new System.Drawing.Size(113, 36);
            this.toDefaultButton.TabIndex = 25;
            this.toDefaultButton.Text = "Default";
            this.toDefaultButton.UseVisualStyleBackColor = false;
            this.toDefaultButton.Click += new System.EventHandler(this.toDefaultButton_Click);
            // 
            // pathBox
            // 
            this.pathBox.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.pathBox.ForeColor = System.Drawing.Color.Black;
            this.pathBox.Location = new System.Drawing.Point(11, 298);
            this.pathBox.Name = "pathBox";
            this.pathBox.Size = new System.Drawing.Size(500, 27);
            this.pathBox.TabIndex = 20;
            this.pathBox.Tag = "";
            this.pathBox.Text = ".";
            // 
            // pathLabel
            // 
            this.pathLabel.AutoSize = true;
            this.pathLabel.BackColor = System.Drawing.Color.Transparent;
            this.pathLabel.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.pathLabel.ForeColor = System.Drawing.Color.Black;
            this.pathLabel.Location = new System.Drawing.Point(12, 275);
            this.pathLabel.Name = "pathLabel";
            this.pathLabel.Size = new System.Drawing.Size(89, 20);
            this.pathLabel.TabIndex = 21;
            this.pathLabel.Text = "FreeSO path:";
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(120)))), ((int)(((byte)(174)))));
            this.saveButton.FlatAppearance.BorderSize = 0;
            this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveButton.Font = new System.Drawing.Font("Segoe UI Light", 24F);
            this.saveButton.ForeColor = System.Drawing.Color.White;
            this.saveButton.Location = new System.Drawing.Point(203, 331);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(123, 55);
            this.saveButton.TabIndex = 18;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // langList
            // 
            this.langList.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.langList.ForeColor = System.Drawing.Color.Black;
            this.langList.FormattingEnabled = true;
            this.langList.Items.AddRange(new object[] {
            "English",
            "Finnish",
            "German",
            "Norwegian",
            "Portuguese",
            "Spanish"});
            this.langList.Location = new System.Drawing.Point(11, 244);
            this.langList.Name = "langList";
            this.langList.Size = new System.Drawing.Size(500, 28);
            this.langList.TabIndex = 24;
            // 
            // ipAddrBox
            // 
            this.ipAddrBox.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.ipAddrBox.ForeColor = System.Drawing.Color.Black;
            this.ipAddrBox.Location = new System.Drawing.Point(11, 57);
            this.ipAddrBox.Name = "ipAddrBox";
            this.ipAddrBox.Size = new System.Drawing.Size(500, 27);
            this.ipAddrBox.TabIndex = 16;
            this.ipAddrBox.Text = "173.248.136.133";
            // 
            // resLabel
            // 
            this.resLabel.AutoSize = true;
            this.resLabel.BackColor = System.Drawing.Color.Transparent;
            this.resLabel.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.resLabel.ForeColor = System.Drawing.Color.Black;
            this.resLabel.Location = new System.Drawing.Point(12, 168);
            this.resLabel.Name = "resLabel";
            this.resLabel.Size = new System.Drawing.Size(76, 20);
            this.resLabel.TabIndex = 22;
            this.resLabel.Text = "Resolution:";
            // 
            // resList
            // 
            this.resList.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.resList.ForeColor = System.Drawing.Color.Black;
            this.resList.FormattingEnabled = true;
            this.resList.Items.AddRange(new object[] {
            "1920x1080",
            "1280x720",
            "1024x768",
            "800x600",
            "420x420"});
            this.resList.Location = new System.Drawing.Point(11, 191);
            this.resList.Name = "resList";
            this.resList.Size = new System.Drawing.Size(500, 28);
            this.resList.TabIndex = 23;
            // 
            // ipLabel
            // 
            this.ipLabel.AutoSize = true;
            this.ipLabel.BackColor = System.Drawing.Color.Transparent;
            this.ipLabel.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.ipLabel.ForeColor = System.Drawing.Color.Black;
            this.ipLabel.Location = new System.Drawing.Point(12, 34);
            this.ipLabel.Name = "ipLabel";
            this.ipLabel.Size = new System.Drawing.Size(77, 20);
            this.ipLabel.TabIndex = 17;
            this.ipLabel.Text = "IP Address:";
            // 
            // langLabel
            // 
            this.langLabel.AutoSize = true;
            this.langLabel.BackColor = System.Drawing.Color.Transparent;
            this.langLabel.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.langLabel.ForeColor = System.Drawing.Color.Black;
            this.langLabel.Location = new System.Drawing.Point(12, 221);
            this.langLabel.Name = "langLabel";
            this.langLabel.Size = new System.Drawing.Size(73, 20);
            this.langLabel.TabIndex = 32;
            this.langLabel.Text = "Language:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Firebrick;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(527, 31);
            this.panel1.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(527, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "WARNING! Only touch these options if you know what you\'re doing.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // launchButton
            // 
            this.launchButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(120)))), ((int)(((byte)(174)))));
            this.launchButton.FlatAppearance.BorderSize = 0;
            this.launchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.launchButton.Font = new System.Drawing.Font("Segoe UI Light", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.launchButton.ForeColor = System.Drawing.Color.White;
            this.launchButton.Location = new System.Drawing.Point(11, 331);
            this.launchButton.Name = "launchButton";
            this.launchButton.Size = new System.Drawing.Size(186, 55);
            this.launchButton.TabIndex = 19;
            this.launchButton.Text = "Close";
            this.launchButton.UseVisualStyleBackColor = false;
            this.launchButton.Click += new System.EventHandler(this.launchButton_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(120)))), ((int)(((byte)(174)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI Light", 24F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(332, 331);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 55);
            this.button1.TabIndex = 34;
            this.button1.Text = "Theme";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(7, 389);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(504, 46);
            this.label2.TabIndex = 35;
            this.label2.Text = "Layout inspired by a concept of Pisarz1958\'s. Code by LetsRaceBwoi.";
            // 
            // advanced
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(527, 412);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.langLabel);
            this.Controls.Add(this.portLabel);
            this.Controls.Add(this.portBox);
            this.Controls.Add(this.toDefaultButton);
            this.Controls.Add(this.pathBox);
            this.Controls.Add(this.pathLabel);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.langList);
            this.Controls.Add(this.ipAddrBox);
            this.Controls.Add(this.resLabel);
            this.Controls.Add(this.resList);
            this.Controls.Add(this.ipLabel);
            this.Controls.Add(this.launchButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "advanced";
            this.Text = "advanced";
            this.Load += new System.EventHandler(this.advanced_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label portLabel;
        private System.Windows.Forms.TextBox portBox;
        private System.Windows.Forms.Button toDefaultButton;
        private System.Windows.Forms.TextBox pathBox;
        private System.Windows.Forms.Label pathLabel;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.ComboBox langList;
        private System.Windows.Forms.TextBox ipAddrBox;
        private System.Windows.Forms.Label resLabel;
        private System.Windows.Forms.ComboBox resList;
        private System.Windows.Forms.Label ipLabel;
        private System.Windows.Forms.Label langLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button launchButton;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.Label label2;

    }
}