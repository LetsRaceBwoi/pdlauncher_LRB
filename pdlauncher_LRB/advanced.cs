﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Management;
using System.Diagnostics;
using System.Windows.Forms;

namespace pdlauncher_LRB
{
    public partial class advanced : Form
    {
        private readonly launcherForm launchfrm;
        public advanced(launcherForm launchfrm)
        {
            InitializeComponent();
            this.launchfrm = launchfrm;
        }

        

        private void advanced_Load(object sender, EventArgs e)
        {
            try
            {
                string[] lines = File.ReadAllLines(pathBox.Text + @"\FreeSO.exe.config");
                ipAddrBox.Text = lines[64].Replace("                <value>", "").Replace("</value>", "");
                portBox.Text = lines[16].Replace("                <value>", "").Replace("</value>", "");
                resList.Text = lines[70].Replace("                <value>", "").Replace("</value>", "") + "x" + lines[73].Replace("                <value>", "").Replace("</value>", "");
                langList.Text = lines[13].Replace("                <value>", "").Replace("</value>", "");
                getStream();
            }
            catch
            {
#if DEBUG
                    throw new Exception();
#endif
            }
        }

        /// <summary>
        /// Get the server status
        /// </summary>
        public void getStream()
        {
            try
            {
                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create("http://" + ipAddrBox.Text + ":8888/city");
                Stream objStream;
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream);
                string sLine = "";
            }
            catch
            {
                //error!
            }
        }

        /// <summary>
        /// Set the program configuration
        /// </summary>
        private void setStream()
        {
            try
            {
                string[] lines = File.ReadAllLines(pathBox.Text + @"\FreeSO.exe.config");
                lines[64] = "                <value>" + ipAddrBox.Text + "</value>";
                lines[16] = "                <value>" + portBox.Text + "</value>";
                lines[70] = "                <value>" + resList.Text.Remove(resList.Text.IndexOf("x"), resList.Text.Length - resList.Text.IndexOf("x")) + "</value>";
                lines[73] = "                <value>" + resList.Text.Remove(0, resList.Text.Length - resList.Text.IndexOf("x")).Replace("x", "") + "</value>";
                lines[13] = "                <value>" + langList.Text + "</value>";
                File.WriteAllLines(pathBox.Text + @"\FreeSO.exe.config", lines);
                variables.path = pathBox.Text;
            }
            catch
            {
                MessageBox.Show("There was a problem doing that! Do you have the right path?");
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            getStream();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            variables.ip = ipAddrBox.Text;
            variables.port = portBox.Text;
            launchfrm.getStream();
            setStream();
            this.Close();
        }

        private void toDefaultButton_Click(object sender, EventArgs e)
        {
            ipAddrBox.Text = "173.248.136.133";
            portBox.Text = "2106";
            button3_Click(toDefaultButton, null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //
        }

        private void launchButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
