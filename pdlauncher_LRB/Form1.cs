﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Management;
using System.Diagnostics;
using System.Windows.Forms;

namespace pdlauncher_LRB
{
    public partial class launcherForm : Form
    {
        int scrollspeed = 10;
        bool mousedown = false;
        int msx = 0;
        int msy = 0;
        string themeid = "tso";

        System.Resources.ResourceManager resources;
        public launcherForm()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Folder containing FreeSO";
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "";
            openFileDialog1.Multiselect = true;
            openFileDialog1.ShowDialog();
        }


        private void button2_Click(object sender, EventArgs e)
        {
        }

        public void refresh()
        {
            try
            {
                string[] lines = File.ReadAllLines(variables.path + @"\FreeSO.exe.config");
                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create("http://" + lines[73] + ":8888/city");
                Stream objStream;
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream);


                serverQueryLabel.Text = objReader.ReadToEnd().Replace("<b>", "").Replace("</b>", "").Replace("</br>", Environment.NewLine).Replace(@"<a href=""http://nancyfx.org"">", "").Replace("</a>", "");
            }
            catch
            {
                serverQueryLabel.Text = "Error getting server status." + Environment.NewLine + "Does the server exist?";

            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mousedown = true;
            msx = e.X;
            msy = e.Y;
        }

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            mousedown = false;
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (mousedown)
            {
                this.Location = new Point(this.Location.X + (e.X - msx), this.Location.Y + (e.Y - msy));
            }
        }

        private void panel2_MouseLeave(object sender, EventArgs e)
        {
            mousedown = false;
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            advanced adv = new advanced(this);
            adv.Visible = !adv.Visible;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            advanced adv = new advanced(this);
            adv.Show();

        }

        public void setTheme()
        {
            if (themeid.Equals("tso"))
            {
                themeid = "blk";
                this.BackgroundImage = null;
                this.BackColor = Color.Black;
                topPanel.BackColor = Color.Firebrick;
                exitButton.BackColor = Color.Firebrick;
                launchButton.BackColor = Color.Firebrick;
                refreshButton.BackColor = Color.Firebrick;
                //advButton.BackColor = Color.Firebrick;
                themeButton.BackColor = Color.Firebrick;
                foreach (Control child in this.Controls)
                {
                    if (child is Label || child is Button || child.Name.EndsWith("Button") || child.Name.EndsWith("Label"))
                    {
                        child.ForeColor = Color.White;
                    }
                }
            }
            else
            {
                if (themeid.Equals("blk"))
                {
                    themeid = "tso";
                    this.BackgroundImage = null;
                    this.BackColor = Color.Black;
                    topPanel.BackColor = Color.FromArgb(80, 120, 174);
                    exitButton.BackColor = Color.FromArgb(80, 120, 174);
                    launchButton.BackColor = Color.FromArgb(80, 120, 174);
                    refreshButton.BackColor = Color.FromArgb(80, 120, 174);
                    //advButton.BackColor = Color.FromArgb(80, 120, 174);
                    themeButton.BackColor = Color.FromArgb(80, 120, 174);
                    foreach (Control child in this.Controls)
                    {
                        if (child is Label || child is Button || child.Name.EndsWith("Button") || child.Name.EndsWith("Label"))
                        {
                            child.ForeColor = Color.Khaki;
                        }
                    }
                }
            }
        }

        private void launcherForm_Load(object sender, EventArgs e)
        {
            //timer1.Start();
            try
            {
                string[] lines = File.ReadAllLines(variables.path + @"\FreeSO.exe.config");
                variables.ip = lines[64].Replace("                <value>", "").Replace("</value>", "");
                variables.port = lines[16].Replace("                <value>", "").Replace("</value>", "");
                getStream();

                string[] imagelist = {
                                         "http://i.imgur.com/7fBVdBFl.jpg",
                                         "http://images34.fotosik.pl/177/aa31a6c2f9425be5.jpg",
                                         "http://images28.fotosik.pl/176/20eb284050b830d4.jpg",
                                         "http://images33.fotosik.pl/177/20efc6196ee0c306.jpg",
                                         "http://images25.fotosik.pl/174/fdcd996b587ef7d4.jpg",
                                         "http://images29.fotosik.pl/175/730950c0135caec3.jpg"
                                     };

                Random randint = new Random();
                int random_selection = randint.Next(imagelist.Length);
                //pictureBox1.Load(imagelist[random_selection]);
                var rq = WebRequest.Create(imagelist[random_selection]);

                using (var rp = rq.GetResponse())
                using (var st = rp.GetResponseStream())
                {
                    pictureBox1.BackgroundImage = Bitmap.FromStream(st);
                }
            }
            catch
            {
#if DEBUG
                    throw new Exception();
#endif
            }

        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            getStream();
        }


        /// <summary>
        /// Get the server status
        /// </summary>
        public void getStream()
        {
            try
            {
                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create("http://" + variables.ip + ":8888/city");
                Stream objStream;
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream);
                string sLine = "";
                sLine = objReader.ReadToEnd();
                serverQueryLabel.Text = sLine.Replace("<b>", "").Replace("</b>", "").Replace(@"</br></br><b>Proudly powered by</b> <a href=""http:/"", "").Replace(@""/nancyfx.org/"">Nancy</a></br>", "").Replace("</br>", Environment.NewLine);
            }
            catch
            {
                //error!
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            String btnTxt = btn.Text;
            btn.Text = "Fetching latest build...";
            btn.Refresh();
            string url = "http://servo.freeso.org/externalStatus.html?js=1";
            WebRequest wrGETURL;
            wrGETURL = WebRequest.Create(url);
            Stream objStream;
            objStream = wrGETURL.GetResponse().GetResponseStream();
            StreamReader objReader = new StreamReader(objStream);
            string sLine = "";
            string fll;
            fll = objReader.ReadLine();
            sLine = fll.Remove(0, 855);
            sLine = sLine.Remove(sLine.IndexOf("</a>"));
            if (MessageBox.Show("The latest build is " + sLine + " - Would you like to download it?", "LRB FreeSO Launcher", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                btn.Text = "Downloading latest build...";
                btn.Refresh();
                WebClient wc = new WebClient();
                wc.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36");
                string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes("guest:"));
                wc.Headers[HttpRequestHeader.Authorization] = string.Format(
                    "Basic {0}", credentials);
                wc.DownloadString("http://servo.freeso.org/guestLogin.html?guest=1");
                //MessageBox.Show("http://servo.freeso.org/repository/download/ProjectDollhouse_TsoClient/" + (Convert.ToInt16(sLine) + 61) + ":id/dist-" + sLine + ".zip");
                wc.DownloadFile("http://servo.freeso.org/repository/download/ProjectDollhouse_TsoClient/" + (Convert.ToInt32(sLine) + 61) + ":id/dist-" + sLine + ".zip", "temp.zip");
                //wc.DownloadFile("http://servo.freeso.org/repository/download/ProjectDollhouse_TsoClient/154:id/dist-93.zip", "temp.zip");
                try
                {
                    ZipFile.ExtractToDirectory("temp.zip", ".");
                }
                catch
                {
                    IReadOnlyCollection<ZipArchiveEntry> entries_ = ZipFile.OpenRead("temp.zip").Entries;
                    foreach (ZipArchiveEntry entry_ in entries_)
                    {
                        entry_.ExtractToFile(entry_.FullName, true);
                    }
                }
            }
            btn.Text = btnTxt;
            btn.Refresh();
        }

        private void launchButton_Click(object sender, EventArgs e)
        {
            Process process1 = new Process();
            process1.StartInfo.FileName = (variables.path + @"\FreeSO.exe");
            process1.Start();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
